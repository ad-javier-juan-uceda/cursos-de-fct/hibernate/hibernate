/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.cipfpbatoi.empresa;

import es.cipfpbatoi.dao.DepartamentoDAO;
import es.cipfpbatoi.dao.Empleado;
import es.cipfpbatoi.dao.EmpleadoDAO;
import es.cipfpbatoi.dao.Tipo;
import es.cipfpbatoi.dao.TipoDAO;
import java.util.List;
import java.util.ArrayList;
import java.util.HashSet;

/**
 *
 * @author sergio
 */
public class App {

    public static void main(String[] args) {
        System.out.println("APP - EMPRESA CON HIBERNATE");

        try {
            EmpleadoDAO empleadoDAO = new EmpleadoDAO();
            TipoDAO tipoDAO = new TipoDAO();
            DepartamentoDAO departamentoDAO = new DepartamentoDAO();
            for (Tipo tipo : tipoDAO.findAll()) {
                System.out.println(tipo);
            }
            Tipo tipo = tipoDAO.findByPK(1);
            System.out.println(tipoDAO.findByPK(1).getNombre());
            tipo = tipoDAO.merge(tipo);
            System.out.println(tipoDAO.findByPK(1).getNombre());
            tipo.setNombre("A");
            tipo = tipoDAO.merge(tipo);
            System.out.println(tipoDAO.findByPK(1).getNombre());
            tipoDAO.update(tipo);
            System.out.println(tipoDAO.findByPK(1).getNombre());
            Empleado empleado = empleadoDAO.findByPK(1);
            System.out.println(empleado);

            empleado = new Empleado();
            empleado.setNombre("J2");
            empleado.setDepartamento(departamentoDAO.findByPK(1));
            empleadoDAO.insert(empleado);
            tipoDAO.anyadir_departamento(tipo, 2);
            tipoDAO.delete_departamento(tipo, 2);
            tipoDAO.insertLotes(crearUnMontonObjetos(10));
            System.out.println(tipoDAO.maximo());
            System.out.println(tipoDAO.minimo());
            System.out.println(tipoDAO.contar());
            System.out.println(tipoDAO.sumar());
            System.out.println(tipoDAO.avg());
            System.out.println(tipoDAO.concat());
            System.out.println(tipoDAO.cadenas());
            tipoDAO.parteTabla();
            tipoDAO.procedimiento();
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Error en alguna operación del programa..." + e.getMessage());
        }

    }

    private static List<Tipo> crearUnMontonObjetos(int numero) {
        List<Tipo> listatipos = new ArrayList();
        for (int contador = 0; contador < numero; contador++) {
            System.out.println(contador + 1);
            Tipo tipo = new Tipo();
            tipo.setNombre(String.valueOf(contador));
            tipo.setDepartamentos(new HashSet(0));
            listatipos.add(tipo);
        }
        System.out.println("Insertando");
        return listatipos;
    }

}
