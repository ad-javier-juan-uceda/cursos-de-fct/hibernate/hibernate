/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.cipfpbatoi.dao;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.Stream;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;
import org.hibernate.SessionFactory;

/**
 *
 * @author Usuario
 */
public class TipoDAO implements GenericoDAO<Tipo> {

    private static EntityManager sessionFactory;
    private static final String SELECCIONAR_TODO = "FROM Tipo";
    private static final String SELECCIONAR_ID = "FROM Tipo WHERE id = :id";

    public TipoDAO() throws Exception {
        sessionFactory = ConexionHB.getSessionFactory();
    }

    @Override
    public Tipo findByPK(int id) throws Exception {
        //return (Tipo) sessionFactory.createQuery("FROM Tipo WHERE id = " + id).getResultList().get(0);
        return (Tipo) sessionFactory.createQuery(SELECCIONAR_ID).setParameter("id", id).getResultList().get(0);
    }

    @Override
    public List<Tipo> findAll() throws Exception {
        //return sessionFactory.createQuery("FROM Tipo").getResultList();
        return sessionFactory.createQuery(SELECCIONAR_TODO).getResultList();
    }

    @Override
    public List<Tipo> findByExample(Tipo t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Tipo> findBySQL(String sqlselect) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public long contar() throws Exception {
        return (long) sessionFactory.createQuery("select count(*) from Tipo").getResultList().get(0);
    }

    public int minimo() throws Exception {
        return (int) sessionFactory.createQuery("select min(id) from Tipo").getResultList().get(0);
    }

    public int maximo() throws Exception {
        return (int) sessionFactory.createQuery("select max(id) from Tipo").getResultList().get(0);
    }

    public long sumar() throws Exception {
        return (long) sessionFactory.createQuery("select sum(id) from Tipo").getResultList().get(0);
    }

    public double avg() throws Exception {
        return (double) sessionFactory.createQuery("select avg(id) from Tipo").getResultList().get(0);
    }

    public List concat() throws Exception {
        return sessionFactory.createQuery("SELECT CONCAT(p.nombre || ' ' || p.id) FROM Tipo p").getResultList();
    }

    public List cadenas() throws Exception {
        return sessionFactory.createQuery("SELECT CONCAT(UPPER(p.nombre) || ' ' || LOWER(p.nombre) || ' ' || TRIM(p.nombre) || ' ' || LENGTH(p.nombre)) FROM Tipo p").getResultList();
    }

    public void parteTabla() throws Exception {
        List<Object[]> registros = sessionFactory.createQuery("select id, nombre from Tipo", Object[].class).getResultList();

        registros.forEach((reg) -> {
            System.out.println(" - Nombre: " + reg[0]);
        });

    }

    public void procedimiento() throws Exception {
        StoredProcedureQuery query = sessionFactory.createStoredProcedureQuery("sumar");
        query.registerStoredProcedureParameter(1, Integer.class, ParameterMode.IN);
        query.registerStoredProcedureParameter(2, Integer.class, ParameterMode.IN);
        query.setParameter(1, 3);
        query.setParameter(2, 5);

        query.execute();
        //List<Object[]> postComments = query.getResultList();
        List<Object[]> postComments = query.getResultList();
        //postComments.forEach((reg) -> {
        //    System.out.println(" - Nombre: " + reg[0]);
        //});
        System.out.println(" - Nombre: " + postComments);

    }

    @Override
    public boolean insert(Tipo t) throws Exception {
        sessionFactory.getTransaction().begin();
        sessionFactory.persist(t);
        sessionFactory.getTransaction().commit();
        return true;
    }

    @Override
    public boolean update(Tipo t) throws Exception {
        sessionFactory.getTransaction().begin();
        Tipo tipo = findByPK(t.getId());
        tipo.setNombre(t.getNombre());
        tipo.setDepartamentos(t.getDepartamentos());
        sessionFactory.persist(tipo);
        sessionFactory.getTransaction().commit();
        return true;
    }

    @Override
    public boolean delete(int id) throws Exception {
        sessionFactory.getTransaction().begin();
        Tipo tipo = findByPK(id);
        sessionFactory.remove(tipo);
        sessionFactory.getTransaction().commit();
        return true;
    }

    public boolean delete_departamento(Tipo tipo, int id) throws Exception {
        tipo.getDepartamentos().remove(new DepartamentoDAO().findByPK(id));
        update(tipo);
        return true;
    }

    public boolean anyadir_departamento(Tipo tipo, int id) throws Exception {

        tipo.getDepartamentos().add(new DepartamentoDAO().findByPK(id));
        update(tipo);
        return true;
    }

    @Override
    public Tipo merge(Tipo objeto) throws Exception {
        return sessionFactory.merge(objeto);
    }

    @Override
    public boolean insertLotes(List<Tipo> lista_objetos) throws Exception {
        sessionFactory.getTransaction().begin();

        for (Tipo tipo : lista_objetos) {
            //flush a batch of inserts and release memory
            sessionFactory.flush();
            sessionFactory.clear();
            sessionFactory.persist(tipo);
        }

        sessionFactory.getTransaction().commit();
        return true;
    }

}
