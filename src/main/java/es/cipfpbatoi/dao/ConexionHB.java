package es.cipfpbatoi.dao;

import javax.persistence.EntityManager;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class ConexionHB {
    // A SessionFactory is set up once for an application!
    private static SessionFactory sessionFactory;

    public static EntityManager getSessionFactory() throws Exception {
        // configure() -> configures settings from hibernate.cfg.xml
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
        try {
            sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        } catch (Exception e) {
            // The registry would be destroyed by the SessionFactory, 
            // but we had trouble building the SessionFactory -> so destroy it manually.
            StandardServiceRegistryBuilder.destroy( registry );
        }
        return sessionFactory.createEntityManager();
    }
}
