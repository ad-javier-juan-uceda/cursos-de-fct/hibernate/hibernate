/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.cipfpbatoi.dao;

import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.SessionFactory;

/**
 *
 * @author Usuario
 */
public class DepartamentoDAO implements GenericoDAO<Departamento> {
    
    private static EntityManager sessionFactory;
    private static final String SELECCIONAR_TODO="FROM Departamento";
    private static final String SELECCIONAR_ID="FROM Departamento WHERE id = :id";
    
    public DepartamentoDAO() throws Exception{
        sessionFactory = ConexionHB.getSessionFactory();
    }

    @Override
    public Departamento findByPK(int id) throws Exception {
        //return (Departamento) sessionFactory.createQuery("FROM Departamento WHERE id = " + id).getResultList().get(0);
        return (Departamento) sessionFactory.createQuery(SELECCIONAR_ID).setParameter( "id", id ).getResultList().get(0);
    }

    @Override
    public List<Departamento> findAll() throws Exception {
        //return sessionFactory.createQuery("FROM Departamento").getResultList();
        return sessionFactory.createQuery(SELECCIONAR_TODO).getResultList();
    }

    @Override
    public List<Departamento> findByExample(Departamento t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Departamento> findBySQL(String sqlselect) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean insert(Departamento t) throws Exception {
        sessionFactory.getTransaction().begin();
        sessionFactory.persist(t);
        sessionFactory.getTransaction().commit();
        return true;
    }

    @Override
    public boolean update(Departamento t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(int id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Departamento merge(Departamento objeto) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean insertLotes(List<Departamento> lista_objetos) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
