/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.cipfpbatoi.dao;

import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author Usuario
 */
public class EmpleadoDAO implements GenericoDAO<Empleado>{
    
    private static EntityManager sessionFactory;
    private static final String SELECCIONAR_TODO="FROM Empleado";
    private static final String SELECCIONAR_ID="FROM Empleado WHERE id = :id";
    
    public EmpleadoDAO() throws Exception{
        sessionFactory = ConexionHB.getSessionFactory();
    }

    @Override
    public Empleado findByPK(int id) throws Exception {
        //return (Empleado) sessionFactory.createQuery("FROM Empleado WHERE id = " + id).getResultList().get(0);
        return (Empleado) sessionFactory.createQuery(SELECCIONAR_ID).setParameter( "id", id ).getResultList().get(0);
    }

    @Override
    public List<Empleado> findAll() throws Exception {
        //return sessionFactory.createQuery("FROM Tipo").getResultList();
        return sessionFactory.createQuery(SELECCIONAR_TODO).getResultList();
    }

    @Override
    public List<Empleado> findByExample(Empleado t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Empleado> findBySQL(String sqlselect) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean insert(Empleado t) throws Exception {
        sessionFactory.getTransaction().begin();
        sessionFactory.persist(t);
        sessionFactory.getTransaction().commit();
        return true;
    }

    @Override
    public boolean update(Empleado t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(int id) throws Exception {
        sessionFactory.getTransaction().begin();
        Empleado tipo = findByPK(id);
        sessionFactory.remove(tipo);
        sessionFactory.getTransaction().commit();
        return true;
    }

    @Override
    public Empleado merge(Empleado objeto) throws Exception {
        return sessionFactory.merge(objeto);
    }

    @Override
    public boolean insertLotes(List<Empleado> lista_objetos) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
